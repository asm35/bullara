// ---
// Marc MELKONIAN
// M2 Bioinformatique (IBI) - Université Rennes 1
// BS1 - Simulation des systèmes biologiques
// Creation date: 02/10/2019
//
// This present script aims to reproduce the two figures (Fig. 1, Fig. 2) of an
// article by Bullara(1) simulating the generation of the striped patterns of
// the zebrafish skin by a Monte Carlo simulation of a Reaction-Diffusion
// mechanism.
//
// (1) Bullara D, De Decker Y. Pigment cell movement is not required for
// generation of Turing patterns in zebrafish skin. Nat Commun. 2015 May
// 11;6:6971. doi: 10.1038/ncomms7971. PubMed PMID: 25959141; PubMed Central
// PMCID: PMC4432648.
//
// IMPORTANT: this program is too slow for RUNS > 1e7 and is superseded by
// the C version
// ---


// global vars ~~~

// lattice
global SIDE ; SIDE = 100  // lattice is a 100x100 square
global L                  // lattice matrix ; we make it a global var to avoid
                          //   copies inside functions calls/returns
global RUNS ; RUNS = 1e8  // nb of iterations for the Monte Carlo simulation

// chromatophores ids
global SEMATOPHORE_ID ; SEMATOPHORE_ID = 1
global MELANOPHORE_ID ; MELANOPHORE_ID = 2
global XANTOPHORE_ID  ; XANTOPHORE_ID = 3

// changing states interactions
bx = 1 ; bm = 0  // births rates
dx = 0 ; dm = 0  // deaths rates
sx = 1 ; sm = 1  // close-by mutual competitions rates

// interactions
global EVENTS ; EVENTS = ["bx", "bm", "dx", "dm", "sm", "sx", "lx"]

// graphics export directory
global EXPORTS_DIR ; EXPORTS_DIR = "exports"


// functions ~~~

function [] = bx_mod(x, y)
    // Si -> Xi (birth of xantophores)
    global L, SEMATOPHORE_ID, XANTOPHORE_ID, MELANOPHORE_ID
    if L(x, y) == SEMATOPHORE_ID then
        L(x, y) = XANTOPHORE_ID
    end
endfunction

function [] = bm_mod(x, y)
    // Si -> Mi (birth of melanophores)
    global L, SEMATOPHORE_ID, XANTOPHORE_ID, MELANOPHORE_ID
    if L(x, y) == SEMATOPHORE_ID then
        L(x, y) = MELANOPHORE_ID
    end
endfunction

function [] = dx_mod(x, y)
    // Xi -> Si (death of xantophores)
    global L, SEMATOPHORE_ID, XANTOPHORE_ID, MELANOPHORE_ID
    if L(x, y) == XANTOPHORE_ID then
        L(x, y) = SEMATOPHORE_ID
    end
endfunction

function [] = dm_mod(x, y)
    // Mi -> Si (death of melanophores)
    global L, SEMATOPHORE_ID, XANTOPHORE_ID, MELANOPHORE_ID
    if L(x, y) == MELANOPHORE_ID then
        L(x, y) = SEMATOPHORE_ID
    end
endfunction

function [] = sm_mod(x, y, snodes)
    // Mi+/-1 + Xi -> Mi+/-1 + Si
    // (short range melanphore-induced death of xantophores)
    global L, SEMATOPHORE_ID, XANTOPHORE_ID, MELANOPHORE_ID
    if L(x, y) <> XANTOPHORE_ID then
        return
    end
    // pick a random first neighbor
    [snode_x, snode_y] = pick_rd_snode(snodes)
    if L(snode_x, snode_y) == MELANOPHORE_ID then
        L(x, y) = SEMATOPHORE_ID
    end
endfunction

function [] = sx_mod(x, y, snodes)
    // Xi+/-1 + Mi -> Xi+/-1 + Si
    // (short range xantophore-induced death of melanophores)
    global L, SEMATOPHORE_ID, XANTOPHORE_ID, MELANOPHORE_ID
    if L(x, y) <> MELANOPHORE_ID then
        return
    end
    // pick a random first neighbor
    [snode_x, snode_y] = pick_rd_snode(snodes)
    if L(snode_x, snode_y) == XANTOPHORE_ID then
        L(x, y) = SEMATOPHORE_ID
    end
endfunction

function [] = lx_mod(x, y, h)
    // Xi+/-h + Si -> Xi+/-h + Mi
    // (long range xantophore-induced birth of melanophores)
    global L, SEMATOPHORE_ID, XANTOPHORE_ID, MELANOPHORE_ID
    if L(x, y) <> SEMATOPHORE_ID then
        return
    end
    // pick a random distant node
    [lnode_x, lnode_y] = rd_lnode(x, y, h)
    if L(lnode_x, lnode_y) == XANTOPHORE_ID then
        L(x, y) = MELANOPHORE_ID
    end
endfunction

function [snodes] = get_snodes(x, y)
    // get first neighbors of a node
    // each node has a coordinancy of 4, so the first neighbors of the node *
    // are defined by the nodes a, b, c, d below:
    // +-+-+-+-+-+-+
    // | | |d| | | |
    // +-+-+-+-+-+-+
    // | |a|*|c| | |
    // +-+-+-+-+-+-+
    // | | |b| | | |
    // +-+-+-+-+-+-+
    global SIDE
    snodes = []
    // offsets of potential snodes with a coordinancy of 4
    // x,y relative offsets of potential first neighbors
    offsets = [ -1  0 ; 0 1 ; 1 0 ; 0 -1 ]
    potentials = repmat([x y], 4, 1) + offsets
    // filter out nodes with out of bounds coordinates
    //for i = 1:size(potentials)(1)
    for i = 1:4 // numbers of potential nodes
        a = potentials(i, 1)
        b = potentials(i, 2)
        if (a > 0 && b > 0 && a <= SIDE && b <= SIDE)..
        then
            snodes = [snodes; [a b]]
        end
    end
endfunction

function [] = interact(lx, h, x, y, evt)
    // update the lattice with the selected interaction implying a given node
    // coordinates
    select evt
    	case "bx" then bx_mod(x, y)
    	case "bm" then bm_mod(x, y)
    	case "dx" then dx_mod(x, y)
    	case "dm" then dm_mod(x, y)
    	case "sm" then sm_mod(x, y, get_snodes(x, y))
    	case "sx" then sx_mod(x, y, get_snodes(x, y))
    	case "lx" then lx_mod(x, y, h)
    end
endfunction

function [snode_x, snode_y] = pick_rd_snode(snodes)
    // pick a random first neighbor
    n = grand(1, 1, "uin", 1, size(snodes)(1))
    snode_x = snodes(n, 1)
    snode_y = snodes(n, 2)
endfunction

function [lnode_x, lnode_y] = rd_lnode(x, y, h)
    // pick a random distant node located at a given distance
    global SIDE
    angle = rand() * 2 * %pi
    lnode_x = floor(x + h * cos(angle))
    lnode_y = floor(x + h * sin(angle))
    while (lnode_x < 1 || lnode_y < 1 || lnode_x > SIDE || lnode_y > SIDE)..
    then
        angle = rand() * 2 * %pi
        lnode_x = floor(x + h * cos(angle))
        lnode_y = floor(x + h * sin(angle))
    end
endfunction

function [] = export_graphics(lx, h)
    // export graphics to file
    global L, EXPORTS_DIR
    f = gcf()
    // set colors ; each row represents the r,g,b values for the a type of
    // chromatophore whose ID is the corresponding index in the color map
    f.color_map = [1 1 1; 0 0 0; 1 .86 0]  // S, M, X -> white, black, yellow
    fname = sprintf("lx%0.1f_h%d.png", lx, h)
    driver("PNG")
    xinit(EXPORTS_DIR + "/" + fname)
    Matplot(L)
    xend()
endfunction

function [] = monte_carlo(lx, h)
    // Monte Carlo simulation
    global L, SIDE, EVENTS, RUNS
    printf("Computing with params: lx = %0.1f, h = %d\n", lx, h)
    // events probabilities total
    r_total = bx + bm + dx + dm + sm + sx + lx
    // probabilities distribution
    probs = [ bx/r_total, bm/r_total, dx/r_total, dm/r_total,..
    		 sm/r_total, sx/r_total, lx/r_total ]
    // lattice, initialized with only sematophores
    L = ones(SIDE, SIDE)
    // Monte Carlo simulation
    for i = 1:RUNS
    	// pick a random event with respect to the probabilities
    	// frequencies distribution of all the events
    	rd_evt = samplef(1, EVENTS, probs)
    	// pick a random site
    	rd_node = grand(1, 2, "uin", 1, SIDE)
        // update site state with the selected interaction if conditions are met
    	interact(lx, h, rd_node(1,1), rd_node(1,2), rd_evt)
    end
    //fname = sprintf("lx%0.1f_h%d.txt", lx, h)
	//fprintfMat(fname, L, "%lg")
    // generate graphics file
    export_graphics(lx, h)
endfunction


// exports dir ~~~
rv = mkdir(EXPORTS_DIR)
select rv
    case -2 then
    	printf("A file named ""exports"" exists in directory. Exiting.\n")
    	quit()
    case 0 then
    	printf("Error. Can""t create ""exports"" directory. Exiting.\n")
    	quit()
    case 2 then
    	printf("A dir named ""exports"" already exists in directory. Skipping.\n")
end


// main ~~~
// simulations with different parameters
for lx = [ 0.5 1.5 2.5 3.5 ]
    for h = [ 5 10 15 ]
    	monte_carlo(lx, h)
    end
end

quit()
